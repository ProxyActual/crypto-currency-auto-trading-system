package LoggingSystems;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class LogConsoleSystemTest {
    /*@Test
    @DisplayName("Test Log Creation")
    public void LogCreation() throws LoggerNameInUseException, InterruptedException {
        Logger.addLogger("Test", "", 256);
        Logger.logData("Test", "LogCreationTest");
        Thread.sleep(100);
        LogConsoleSystem.LogCommands("Test");
        Logger.killLogger("Test");
    }


    @Test
    @DisplayName("DisplayLogs of Length")
    public void LogDisplayOfLength() throws LoggerNameInUseException, InterruptedException {
        Logger.addLogger("Test", "", 256);
        Logger.logData("Test", "LengthOfLogsTest");
        Thread.sleep(100);
        LogConsoleSystem.LogCommands("Test --get 4");
        Logger.killLogger("Test");
    }


    @Test
    @DisplayName("Display Logs No Limit")
    public void LogNoLimit() throws LoggerNameInUseException, InterruptedException {
        Logger.addLogger("Test", "", 256);
        Logger.logData("Test", "Display with no limits");
        Thread.sleep(100);
        LogConsoleSystem.LogCommands("Test --get");
        Logger.killLogger("Test");
    }


    @Test
    @DisplayName("Change Log Length")
    public void LogChangeMonitorLength() throws LoggerNameInUseException, InterruptedException {
        Logger.addLogger("Test", "", 256);
        Logger.logData("Test", "logs of specified length");
        Thread.sleep(100);
        LogConsoleSystem.LogCommands("Test --size 8");
        int testValue = Logger.getMaxLogLength("Test");
        assert(testValue == 8);
        Logger.killLogger("Test");
    }

    @Test
    @DisplayName("Multi Command with extra Command")
    public void LogDisplayWithChange() throws LoggerNameInUseException, InterruptedException {
        Logger.addLogger("Test", "", 256);
        Logger.logData("Test", "Logs with get and extra command");
        Thread.sleep(100);
        LogConsoleSystem.LogCommands("Test --get --size 8");
        assert(Logger.getMaxLogLength("Test") == 8);
        Logger.killLogger("Test");
    }

    @Test
    @DisplayName("Invalid Command")
    public void LogErrorInDisplay() throws LoggerNameInUseException, InterruptedException {
        Logger.addLogger("Test", "", 256);
        Logger.logData("Test", "Invalid log command");
        Thread.sleep(100);
        LogConsoleSystem.LogCommands("Test --ge ");
        Logger.killLogger("Test");
    }

    @Test
    @DisplayName("Invalid Log")
    public void LogNotValid() throws LoggerNameInUseException, InterruptedException {
        Logger.addLogger("Test", "", 256);
        Logger.logData("Test", "Invalid Log test");
        Thread.sleep(100);
        LogConsoleSystem.LogCommands("TACOS --get");
        Logger.killLogger("Test");
    }


    @Test
    @DisplayName("Invalid Log new Length")
    public void LogNotValidLength() throws LoggerNameInUseException, InterruptedException {
        Logger.addLogger("Test", "", 256);
        Logger.logData("Test", "Invalid lenght test");
        Thread.sleep(100);
        LogConsoleSystem.LogCommands("Test --size sldf");
        Logger.killLogger("Test");
    }


    @Test
    @DisplayName("Calling help")
    public void LogUsingHelp() throws LoggerNameInUseException, InterruptedException {
        Logger.addLogger("Test", "", 256);
        Logger.logData("Test", "Invalid lenght test");
        Thread.sleep(100);
        LogConsoleSystem.LogCommands("help");
        Logger.killLogger("Test");
    }*/
}