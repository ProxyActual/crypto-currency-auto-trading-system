package RestAPIConnections;

import UserInterface.SettingsLoader;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MySQL_ConnectionTest {
    @Test
    @DisplayName("Current Valid BTC")
    public void testForCurrentValidCoin(){
        SettingsLoader.loadSystemSettings();
        MySQL_Connection ms = new MySQL_Connection();
        assert(ms.getCurrentlyPopulatedTimeStamps("BTC", 1618275840).size() > 0);
    }

    @Test
    @DisplayName("Load all current data for System")
    public void testForHistory(){
        SettingsLoader.loadSystemSettings();
        MySQL_Connection ms = new MySQL_Connection();
        assert(ms.getHistoricValuesForCoin("BTC", 1618276020 + 50000, 1618276020).size() > 0);
    }
}