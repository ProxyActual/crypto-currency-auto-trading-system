package RestAPIConnections;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class cexIOapiCommands {
    public static final String[] CURRENCY_LIMITS_VALUES =
            {"pricePrecision", "minLotSize", "minLotSizeS2", "maxLotSize", "minPrice", "maxPrice"};
    public static final String[] PERSONAL_FEES_VALUES =
            {"buy", "sell", "buyMaker", "sellMaker"};
    public static final String COMPARISON_CURRENCY = "USD";

    private static final long DELTA_DELAY_TIME = 1000 * 60 * 60 * 4;

    private static ConcurrentHashMap<String, Long> _validStringPairs;
    private static ConcurrentHashMap<String, Long> _invalidStringPairs;

    public static HashMap<String, Double> getWalletValues() {
        HashMap<String, Double> returnValues = new HashMap<>();
        String walletData = cexIOapi.getWepPage
                ("https://cex.io/api/balance/", cexIOapi.emptyRequestAuthentication() + "\n}");
        String[] splitData = walletData.split(":\\{");
        for (int i = 1; i < splitData.length; i++) {
            String[] coinNameData = splitData[i - 1].split("\"");
            String coinName = coinNameData[coinNameData.length - 1];
            String[] coinData = splitData[i].split(":");
            String walletAmount = coinData[1].split("\"")[1];
            returnValues.put(coinName, Double.parseDouble(walletAmount));
        }
        return returnValues;
    }

    public static String makeOrder(String OriginalCoin, String NewCoin, double amount, double desiredTransfer) {
        String weblink = "https://cex.io/api/place_order/" + OriginalCoin + "/" + NewCoin;
        String postData = cexIOapi.emptyRequestAuthentication() + ",\n" +
                "\"type\":\"buy\",\n" +
                "\"amount\":" + amount + ",\n" +
                "\"price\":" + desiredTransfer + ",\n" + "}";
        String apiResults = cexIOapi.getWepPage(weblink, postData);
        return apiResults;
    }

    public static String getWalletAddress(String walletName) {
        String postData = cexIOapi.emptyRequestAuthentication() + ",\n"
                + "\"currency\": \"" + walletName + "\"\n}";
        return cexIOapi.getWepPage("https://cex.io/api/get_address/", postData);
    }

    public static double getLastPrice(String coin){
        double retVal = -1;
        String webLink = "https://cex.io/api/last_price/" + coin + "/USD";
        String webReturn = cexIOapi.getWepPage(webLink, "");
        String[] retValues = webReturn.split("\"lprice\":\"");
        if(retValues.length >= 2){
            retVal = Double.parseDouble(retValues[1].split("\",")[0]);
        }
        return retVal;
    }

    public static HashMap<String[], double[]> getPersonalFees(){
        HashMap<String[], double[]> returnMap = new HashMap<>();
        String wepPage = "https://cex.io/api/get_myfee/";
        String postData = cexIOapi.emptyRequestAuthentication() + "\n}";
        String webPageReturn = cexIOapi.getWepPage(wepPage, postData);
        String[] splitData = webPageReturn.split("\"data\":\\{\"");
        if(splitData.length >= 2){
            splitData = splitData[1].split("\"},\"");
            for(String data: splitData){
                String[] coinName = data.split("\":")[0].split(":");
                String[] coinData = data.split(":\\{")[1].split("\",\"");
                double[] retValue = new double[4];
                for(int i = 0; i < coinData.length; i++){
                    String doubleParse = coinData[i].split("\":\"")[1];
                    if(doubleParse.contains("\"}}}"))
                        doubleParse = doubleParse.replace("\"}}}", "");
                    retValue[i] = Double.parseDouble(doubleParse);
                }
                returnMap.put(coinName, retValue);
                //System.out.print(data);
            }
        }
        return returnMap;
    }

    public static HashMap<String, double[]> getCurrencyLimits(){
        String webPage = "https://cex.io/api/currency_limits";
        String webResponse = cexIOapi.getWepPage(webPage, "");
        HashMap<String, double[]> retVal = new HashMap<>();
        String coinData = webResponse.split("pairs\":\\[\\{")[1];
        String[] splitCoinData = coinData.split("},\\{");
        for(String coin: splitCoinData){
            String symbol1 = coin.split("\":\"")[1].split("\",")[0];
            String symbol2 = coin.split("\":\"")[2].split("\",")[0];
            double[] returnValues = new double[6];
            String[] coinLimits = coin.split(",\"");

            if(symbol2.matches(COMPARISON_CURRENCY)) {
                for (int i = 0; i < returnValues.length; i++) {
                    String limit = coinLimits[i + 2].split("\":")[1];
                    if (limit.contains("\"") || limit.contains("}") || limit.contains("]")) {
                        limit = limit.replace("\"", "")
                                .replace("}", "").replace("]", "");
                    }
                    if (limit.matches("null")) {
                        returnValues[i] = 0;
                    } else {
                        returnValues[i] = Double.parseDouble(limit);
                    }
                }
                retVal.put(symbol1, returnValues);
            }
        }
        return retVal;
    }

    public static boolean isValidPair(String currency1, String currency2){
        boolean retVal;
        initiatePairsTrackers();
        String pairCode = currency1 + ":" + currency2;
        String inversePairCode = currency2 + ":" + currency1;
        if(_validStringPairs.containsKey(pairCode) &&
                _validStringPairs.get(pairCode) > System.currentTimeMillis()){
            retVal = true;
        }
        else if(_validStringPairs.containsKey(inversePairCode) &&
                        _validStringPairs.get(inversePairCode) > System.currentTimeMillis()){
            retVal = true;
        }
        else if((_invalidStringPairs.containsKey(pairCode) &&
                _invalidStringPairs.get(pairCode) > System.currentTimeMillis())){
            retVal = false;
        }
        else {
            if(apiValidPair(pairCode.split(":"))){
                _validStringPairs.put(pairCode, System.currentTimeMillis() + DELTA_DELAY_TIME);
                retVal = true;
            }
            else if(apiValidPair(inversePairCode.split(":"))){
                _validStringPairs.put(inversePairCode, System.currentTimeMillis() + DELTA_DELAY_TIME);
                retVal = true;
            }
            else{
                _invalidStringPairs.put(pairCode, System.currentTimeMillis() + DELTA_DELAY_TIME);
                retVal = false;
            }
        }
        return retVal;
    }

    private static boolean apiValidPair(String[] coins){
        String webPage = "https://cex.io/api/last_price/";
        String webResponse = cexIOapi.getWepPage(webPage + coins[0] + "/" + coins[1] + "/", "");
        return !webResponse.contains("\"error\"");
    }

    private static void initiatePairsTrackers(){
        if(_validStringPairs == null){
            _validStringPairs = new ConcurrentHashMap<>();
        }
        if(_invalidStringPairs == null){
            _invalidStringPairs = new ConcurrentHashMap<>();
        }
    }
}

