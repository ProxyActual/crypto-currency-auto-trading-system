package RestAPIConnections;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Scanner;

public class cexIOapi {

    private static String _apiToken;
    private static String _userID;
    private static String _secretKey;

    public cexIOapi(String apiToken, String userID, String secretKey){
        _userID = userID;
        _apiToken = apiToken;
        _secretKey = secretKey;
    }
    public cexIOapi(String[] configs){
        _userID = configs[0];
        _apiToken = configs[1];
        _secretKey = configs[2];
    }

    public static boolean isValidConnection(){
        String readResult = getWepPage("https://cex.io/api/balance/", emptyRequestAuthentication() + "\n}");
        return readResult.contains("username");
    }

    public static String getWepPage(String link, String body){
        String retVal = "";
        try{
            HttpURLConnection conn = (HttpURLConnection)(new URL(link)).openConnection();
            postData(conn, body);
            retVal = readWebPageValues(conn);
        }catch(IOException e){
            System.out.println("There was an error in connecting to \n" + link);
        }
        return retVal;
    }

    private static String hmacSha256HEX(String message)
            throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {
        //Prepare the sha256 cipher
        Mac hmacSha256;
        try{
            hmacSha256 = Mac.getInstance("HmacSHA256");
        }catch(NoSuchAlgorithmException nsae){
            hmacSha256 = Mac.getInstance("HMAC-SHA-256");
        }

        //convert the key in to a usable number
        SecretKeySpec secretKeySpec = new SecretKeySpec(_secretKey.getBytes("UTF-8"), "HmacSHA256");
        hmacSha256.init(secretKeySpec);
        String base64String = Base64.getEncoder().encodeToString(hmacSha256.doFinal(message.getBytes("UTF-8")));
        //Convert system in to hex
        byte[] decoded = Base64.getDecoder().decode(base64String);
        return String.format("%40x", new BigInteger(1, decoded)).toUpperCase();
    }

    private static void postData(HttpURLConnection conn, String data) throws ProtocolException {
        String urlParameters = data;
        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
        int postDataLength = postData.length;

        conn.setDoOutput(true);
        conn.setInstanceFollowRedirects(false);
        //makes sure that the data can be sent and that it is sent in the correct formatting
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        conn.setRequestProperty("charset", "utf-8");
        conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        conn.setUseCaches(false);

        //loads the system back from the web page
        try( DataOutputStream wr = new DataOutputStream( conn.getOutputStream())) {
            wr.write(postData);
        } catch (IOException e) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            postData(conn, data);
            //e.printStackTrace();
        }
    }

    public static String emptyRequestAuthentication(){
        String nonce = nonce();
        String retVal = "";
        try{
            retVal =  "{\n" +
                    "\"key\":\""+ _apiToken + "\",\n" +
                    "\"signature\":\"" +  hmacSha256HEX(nonce + _userID + _apiToken) + "\",\n" +
                    "\"nonce\":\""+ nonce + "\"";
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return retVal;
    }

    private static String nonce(){
        return "" + System.currentTimeMillis();
    }

    private static String readWebPageValues(HttpURLConnection UrlConn){
        String retVal = "";
        String tempLine = "";
        try {
            BufferedReader read = new BufferedReader(new InputStreamReader(UrlConn.getInputStream()));
            while ((tempLine = read.readLine()) != null) {
                retVal += tempLine;
            }
            read.close();
        } catch (IOException e) {
            System.err.println("attempting connectionFailing");
        }
        return retVal;
    }
}
