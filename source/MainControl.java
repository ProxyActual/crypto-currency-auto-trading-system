import CryptoControlPackage.CryptoTraderBackend;
import LoggingSystems.Logger;
import NetworkTrainer.TrainerControl;
import NeuralNetwork.MutationNetwork;
import NeuralNetwork.NeuralNetworkCore;
import UserInterface.ConsoleSystem;
import UserInterface.ConsoleSystemStartUp;
import UserInterface.SettingsLoader;

public class MainControl {

    public static void main(String[] args){
        Logger.addLogger("ERROR", "", 255);
        SettingsLoader.loadSystemSettings();
        ConsoleSystemStartUp.validateSettings();

        CryptoTraderBackend cb = new CryptoTraderBackend(
                SettingsLoader.getMonitoredCoins(),
                SettingsLoader.getDeltaTimeSetting(), "");

        try {
            Thread.sleep(60000*15);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        TrainerControl tc = new TrainerControl(
                SettingsLoader.getDeltaTimeSetting(),
                SettingsLoader.getMonitoredCoins());

        TrainerControl.setNeuralNetwork(new MutationNetwork
                (new NeuralNetworkCore
                        (50, 1, SettingsLoader.getMonitoredCoins()).toString()));

        new ConsoleSystem();

        while(!ConsoleSystem.hasExitCall()){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
