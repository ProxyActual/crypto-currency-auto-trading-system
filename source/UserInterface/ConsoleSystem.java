package UserInterface;

import LoggingSystems.LogConsoleSystem;
import NetworkTrainer.TrainerConsoleSystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ConsoleSystem implements Runnable{
    private static Scanner _scanner;
    private static boolean _enabled = true;
    private static Thread _thisThread;

    public ConsoleSystem(){
        _scanner = new Scanner(System.in);
        _thisThread = new Thread(this);
        _thisThread.setName("ConsoleCommands");
        _thisThread.start();
    }

    @Override
    public void run(){
        System.out.println("\nUser inputs are now accepted loading complete\n");
        while(_enabled){
            System.out.print("> ");
            String readLine = _scanner.nextLine();
            useString(readLine);
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean hasExitCall(){
        return !_enabled;
    }

    private void useString(String line){
        ArrayList<String> controlCommands = new ArrayList<>();
        controlCommands.addAll(List.of(line.split(" ")));
        if(controlCommands.size() == 1 && controlCommands.get(0).matches("exit")){
            _enabled = false;
        }
        else if(controlCommands.get(0).matches(LogConsoleSystem.LOG_KEY)){
            controlCommands.remove(0);
            LogConsoleSystem.LogCommands(controlCommands);
        }
        else if(controlCommands.get(0).matches(TrainerConsoleSystem.TRAINER_KEY)){
            controlCommands.remove(0);
            TrainerConsoleSystem.TrainerCommands(controlCommands);
        }
        else{
            System.out.println("Not A valid command");
            help();
        }
    }

    private void help(){
        System.out.println("Current commands");
        System.out.println("\t" + LogConsoleSystem.LOG_KEY + "\t gives the current logs of the system");
        System.out.println("\texit\t ends the auto trading system");
    }


}
