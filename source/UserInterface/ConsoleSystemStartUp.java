package UserInterface;

import RestAPIConnections.cexIOapi;
import RestAPIConnections.cexIOapiCommands;

public class ConsoleSystemStartUp {
    private static final String OTHER_COIN = "USD";
    public static void validateSettings(){
        validateCEX();
        validateCryptos();

    }

    public static void validateCEX(){
        String[] keys = SettingsLoader.getCexKeys();
        if(keys[0]== null){

        }
        if(keys[1]== null){

        }
        if(keys[2]== null){

        }
        new cexIOapi(keys);
        if(!cexIOapi.isValidConnection()){
            System.out.println("There was no valid Connection.");
        }
        else{
            SettingsLoader.setCexIoData(keys);
        }
    }

    public static void validateCryptos(){
        boolean isValidCoins = true;
        for(String coin: SettingsLoader.getMonitoredCoins()){
            isValidCoins = isValidCoins && cexIOapiCommands.isValidPair(coin, OTHER_COIN);
        }
        if(!isValidCoins){
            System.out.println("There is a non valid coin");
        }
    }

    public static void validateDeltaTime(){
        if(SettingsLoader.getDeltaTimeSetting() > 0){
            System.out.println("This isn't a valid time");
        }
    }
}
