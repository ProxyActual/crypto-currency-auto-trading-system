package UserInterface;

import LoggingSystems.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

public class SettingsLoader {
    private static final String FILE_NAME = "cryptoTrader.config";
    private static final String MONITORED_COINS = "monitoredCoins";
    private static final String TIME_DELTA_NEEDED = "timeDeltaForNetwork";
    private static final String[] SQL_CON_DATA = {"MySQL", "MySQL_user", "MySQL_pass"};
    private static final String[] CEX_IO_DATA = {"cexUserName", "cexKeyAPI", "cexSecret"};
    private static final String HISTORY_FILE_LOC = "historyFileLoc";


    private static ConcurrentHashMap<String, Object> _systemSettings;

    public static void loadSystemSettings(){
        loadSystemSettings(System.getProperty("user.dir"));
    }

    public static void loadSystemSettings(String filePath){
        _systemSettings = new ConcurrentHashMap<>();
        Path path = Paths.get(filePath, FILE_NAME);
        try {
            Scanner sc = new Scanner(path);
            while(sc.hasNextLine()){
                String readData = sc.nextLine();
                _systemSettings.put(readData.split(" : ")[0], convertStringToObject(readData));
            }
            sc.close();
        } catch (IOException e) {
            Logger.logData("ERROR", "No config file at " + path);
        }
    }

    public static void saveSystemSettings(){
        saveSystemSettings(System.getProperty("user.dir"));
    }

    public static void saveSystemSettings(String filePath){
        Path path = Paths.get(filePath, FILE_NAME);
        try{
            BufferedWriter bw = new BufferedWriter(new FileWriter(path.toString()));
            for(String key: _systemSettings.keySet()){
                bw.write(convertKeyToString(key) + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void setMonitoredCoins(String[] newCoins){
        _systemSettings.put(MONITORED_COINS, newCoins);
    }

    public static void setCexIoData(String[] newValues){
        for(int i = 0; i < newValues.length; i++){
            _systemSettings.put(CEX_IO_DATA[i], newValues[i]);
        }
    }

    public static void setPersonalFees(HashMap<String[], double[]> newFees){
        _systemSettings.put("personalFees", newFees);
    }

    public static void setCurrencyLimits(HashMap<String, double[]> currencyLimits){
        _systemSettings.put("currencyLimits", currencyLimits);
    }

    public static void setTradeDeltaLookUp(int newDelay){
        _systemSettings.put(TIME_DELTA_NEEDED, newDelay);
    }

    public static HashMap<String[], double[]> getPersonalFees(){
        return (HashMap<String[], double[]>)_systemSettings.get("personalFees");
    }

    public static HashMap<String, double[]> getCurrencyLimits(){
        return (HashMap<String, double[]>)_systemSettings.get("currencyLimits");
    }

    public static String[] getMonitoredCoins(){
        return (String[])_systemSettings.get(MONITORED_COINS);
    }

    public static int getDeltaTimeSetting(){
        return (int)_systemSettings.get(TIME_DELTA_NEEDED);
    }

    public static String[] getSQL_Data(){
        String[] retData = new String[SQL_CON_DATA.length];
        for(int i = 0 ; i < retData.length; i++){
            retData[i] = _systemSettings.contains(SQL_CON_DATA[i]) ?
                    null : _systemSettings.get(SQL_CON_DATA[i]).toString();
        }
        return retData;
    }

    public static String[] getCexKeys(){
        String[] returnData = new String[CEX_IO_DATA.length];
        for(int i = 0; i < returnData.length; i++){
            returnData[i] = _systemSettings.contains(CEX_IO_DATA[i])?
                    null:_systemSettings.get(CEX_IO_DATA[i]).toString();
        }
        return returnData;
    }

    private static Object convertStringToObject(String readLine){
        Object retVal = null;
        if(readLine.split(" : ")[0].matches(MONITORED_COINS)){
            retVal = readLine.split(" : ")[1].split(",");
        }
        else if(readLine.split(" : ")[0].matches(TIME_DELTA_NEEDED)){
            retVal = Integer.parseInt(readLine.split(" : ")[1]);
        }
        else{
            retVal = readLine.split(" : ")[1];
        }
        return retVal;
    }

    private static String convertKeyToString(String key){
        Object ob = _systemSettings.get(key);
        if(key.matches(TIME_DELTA_NEEDED)){
            return key + " : " + (int)ob;
        }
        else if(key.matches(MONITORED_COINS)){
            String[] coins = (String[])ob;
            String coinBrick = coins[0];
            for(int i = 1; i < coins.length; i++){
                coinBrick = coinBrick + "," + coins[i];
            }
            return key + " : " + coinBrick;
        }
        else{
            return key + " : " + ob.toString();
        }
    }


}
