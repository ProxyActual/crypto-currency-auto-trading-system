package LoggingSystems;

public class LoggerNameInUseException extends Exception{
    public LoggerNameInUseException(String errorMessage){
        super(errorMessage);
    }
}
