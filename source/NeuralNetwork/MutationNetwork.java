package NeuralNetwork;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * @author Curtis Andersen
 * This is the mutation based system for the neural networks that will ultimitlay lead to the trading of the system.
 * For the most part this will only be used in the trainer of the network.
 */
public class MutationNetwork extends NeuralNetworkCore{
    public static final int REMOVE_CONNECTION = 0;
    public static final int REMOVE_NODE = 1;
    public static final int EDIT_CONNECTION = 2;
    public static final int NEW_NODE = 3;
    public static final int NEW_CONNECTION = 4;
    public static final int REBUILD_BASE = 5;
    private static final int NUMBER_OF_MUTATIONS = 6;

    private double[] _chances;

    private Random _rand = new Random();

    /**
     * Makes a neural network off of the existing string that would have been saved somewhere or acts as a clean
     * non-linked system.
     * @param existingNet the String of the existing Network
     */
    public MutationNetwork(String existingNet){
        super(existingNet);
        randomFillChances();
    }

    /**
     * Use the preset values to have a chance of mutating the system.
     */
    public void mutateNetwork(){
        makeNewNetwork(_chances[REBUILD_BASE]);
        removeRandomConnection(_chances[REMOVE_CONNECTION]);
        removeRandomNode(_chances[REMOVE_NODE]);
        editRandomConnectionWeight(_chances[EDIT_CONNECTION]);
        addRandomNode(_chances[NEW_NODE]);
        addRandomNewConnection(_chances[NEW_CONNECTION]);
    }

    /**
     * Sets the values that will be used when mutating the system.
     * @param mutationType this is the value for the type of mutation being changed. These are set as public finals
     * @param newChance the new double chance for things to mutate.
     */
    public void setMutationChance(int mutationType, double newChance){
        _chances[mutationType] = newChance;
    }

    public void removeNodesWithName(String coin){
        for(NeuralNetworkNode node: _nodeList.values()){
            if(node.getNodeId().contains(coin)){
                safeRemoveNode(node);
            }
        }
    }

    public void makeNetworkMatchArrayInputs(ArrayList<String> validCoins){
        ArrayList<String> currentMonitoredCoins = getNeuralNetworkCoin();
        for(int i = validCoins.size()-1; i >= 0; i --){
            if(!getNeuralNetworkCoin().contains(validCoins.get(i))){
                addNewCoinInAndOut(validCoins.get(i));
            }
            currentMonitoredCoins.remove(validCoins.get(i));
        }
        for(String removeCoin: currentMonitoredCoins){
            removeNodesWithName(removeCoin);
        }
    }

    private void makeNewNetwork(double chance){
        if(chance(chance)){
            for(String coin: _nodeList.keySet()) {
                if(coin.contains("HIDDEN")){
                    safeRemoveNode(getNodeFromId(coin));
                }
            }
        }
    }

    /**
     * This edits and existing linking weight of two nodes if the chance is successful
     * @param chance this is the chance the connections have of being changed 1.00 = 100%
     */
    private void editRandomConnectionWeight(double chance){
        for(NeuralNetworkNode startingNode: _nodeList.values()){
            for(NeuralNetworkNode endingNode: startingNode.getConnectedNodes()){
                if(chance(chance)){
                    double newEdgeValue = (_rand.nextDouble() / _rand.nextInt()) + startingNode.getNodeConnectionWeight(endingNode);
                    startingNode.addNodeConnection(endingNode, newEdgeValue);
                }
            }
        }
    }

    /**
     * Will add a single new node to the list if the chance is successful
     * @param chance the chance that a new node will be added to the list
     */
    private void addRandomNode(double chance){
        int number = (int)(Math.abs(_rand.nextDouble()) * (double)_nodeList.size()/5);
        for(int i = 0; i < number; i++) {
            if(chance(chance)) {
                ArrayList<NeuralNetworkNode> possibleEndingNodes = getNodesOfType(NeuralNetworkNode.HIDDEN_NODE);
                possibleEndingNodes.addAll(getNodesOfType(NeuralNetworkNode.OUTPUT_NODE));
                ArrayList<NeuralNetworkNode> possibleStartNodes = getNodesOfType(NeuralNetworkNode.HIDDEN_NODE);
                possibleStartNodes.addAll(getNodesOfType(NeuralNetworkNode.INPUT_NODE));
                possibleStartNodes.addAll(getNodesOfType(NeuralNetworkNode.OUTPUT_NODE));
                NeuralNetworkNode node = addNode(getNewId("HIDDEN"), NeuralNetworkNode.HIDDEN_NODE);

                try {
                    for (NeuralNetworkNode nodesTo : possibleEndingNodes) {
                        if (chance(chance)) {
                            nodesTo.addNodeConnection(node, _rand.nextDouble()/_rand.nextInt());
                        }
                    }
                    for (NeuralNetworkNode nodesFrom : possibleStartNodes) {
                        if (chance(chance)) {
                            node.addNodeConnection(nodesFrom, _rand.nextDouble()/_rand.nextInt());
                        }
                    }
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                if (!stillValidNode(node)) {
                    safeRemoveNode(node);
                }
            }
        }
    }

    /**
     * Adds a new connection between the existing nodes it will add the node if the chance is successful
     * @param chance the probability the system will add a new edge between any given nodes
     */
    private void addRandomNewConnection(double chance){
        ArrayList<NeuralNetworkNode> possibleEndingNodes = getNodesOfType(NeuralNetworkNode.HIDDEN_NODE);
        possibleEndingNodes.addAll(getNodesOfType(NeuralNetworkNode.OUTPUT_NODE));

        ArrayList<NeuralNetworkNode> possibleStartNodes = getNodesOfType(NeuralNetworkNode.HIDDEN_NODE);
        possibleStartNodes.addAll(getNodesOfType(NeuralNetworkNode.INPUT_NODE));
        possibleStartNodes.addAll(getNodesOfType(NeuralNetworkNode.OUTPUT_NODE));

        for(NeuralNetworkNode nodeEnding: possibleEndingNodes){
            for(NeuralNetworkNode nodeStarting: possibleStartNodes){
                if(chance(chance)){
                    addConnection(nodeStarting.getNodeId(), nodeEnding.getNodeId(), _rand.nextDouble()/_rand.nextInt());
                }
            }
        }
    }

    /**
     * remove a random hidden node this will not touch the inputs or outputs of the nodes
     * it should be noted that this will also remove any nodes that are connected and not considered valid after
     * this node is removed
     * @param chance the chance any hidden node has to be taken
     */
    private void removeRandomNode(double chance){
        ArrayList<NeuralNetworkNode> nodeList = getNodesOfType(NeuralNetworkNode.HIDDEN_NODE);
        for(NeuralNetworkNode node: nodeList){
            if(chance(chance))
                safeRemoveNode(node);
        }
    }

    /**
     * removes a connection and checks to see if removing that connection un-validated any node and if it did it will
     * remove that node as well
     * @param chance the chance that any connection has to be removed
     */
    private void removeRandomConnection(double chance){
        ArrayList<NeuralNetworkNode> possibleEndingNodes = getNodesOfType(NeuralNetworkNode.HIDDEN_NODE);
        possibleEndingNodes.addAll(getNodesOfType(NeuralNetworkNode.OUTPUT_NODE));
        for(NeuralNetworkNode endingNode: possibleEndingNodes){
            ArrayList<NeuralNetworkNode> startedNode = new ArrayList<>(endingNode.getConnectedNodes());
            for(int i = 0; i < startedNode.size(); i++){
                if(chance(chance)) {
                    safeDeleteConnection(endingNode, startedNode.get(i));
                    startedNode.remove(i);
                    i--;
                }
            }
        }
    }

    //SUPPORTING METHODS

    /**
     * a supporting system that allows for the chance to be easily calculated.
     * @param chanceDouble the chance the system has to return true. 20% is represented by .2
     * @return if the random generated answer is in the percentage
     */
    private boolean chance(double chanceDouble){
        double randGenerated = Math.abs(_rand.nextDouble());
        return randGenerated < chanceDouble;
    }

    /**
     * checks if any node is still valid this is defined as at minimum having two inputs and having one output.
     * @param node the node to check for still valid
     * @return true if the node has the required connections
     */
    private boolean stillValidNode(NeuralNetworkNode node){
        return node.getNodeType() != NeuralNetworkNode.HIDDEN_NODE ||
                (node.getConnectedNodes().size() >= 2 &&  nodesConnectingTo(node).size() >= 1);
    }

    /**
     * safely removes a connection from the neural network.
     * If there is one of the connecting nodes that is no longer valid after the item is removed that item is safely
     * removed as well
     * @param startingNode the node that holds the connection
     * @param endingNode the node that the connection comes to
     */
    private void safeDeleteConnection(NeuralNetworkNode startingNode, NeuralNetworkNode endingNode){
        startingNode.removeConnection(endingNode);
        if(!stillValidNode(startingNode)){
            safeRemoveNode(startingNode);
        }
    }

    /**
     * removes a node and removes all the connections that are connecting to it
     * @param targetNode the node that needs to be removed
     */
    private void safeRemoveNode(NeuralNetworkNode targetNode){
        ArrayList<NeuralNetworkNode> connectedNodes = nodesConnectingTo(targetNode);
        _nodeList.remove(targetNode);
        for(NeuralNetworkNode node: connectedNodes){
            safeDeleteConnection(node, targetNode);
        }
    }

    /**
     * Gets all the nodes that connect to a specified node
     * @param node the node to test
     * @return the list of node that contain a connection to that node
     */
    private ArrayList<NeuralNetworkNode> nodesConnectingTo(NeuralNetworkNode node){
        ArrayList<NeuralNetworkNode> retArray = new ArrayList<>();
        for(NeuralNetworkNode n: _nodeList.values()){
            if(n.getConnectedNodes().contains(node))
                retArray.add(n);
        }
        return retArray;
    }

    /**
     * this will fill the chance that something happens at the start of the system.
     */
    private void randomFillChances(){
        _chances = new double[NUMBER_OF_MUTATIONS];
        for(int i = 0 ; i < _chances.length; i++){
            _chances[i] = _rand.nextFloat() / 10;
        }
    }
}
