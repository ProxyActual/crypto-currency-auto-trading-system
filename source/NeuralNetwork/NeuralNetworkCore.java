package NeuralNetwork;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Curtis Andersen
 * This is the Core network in the system
 */
public class NeuralNetworkCore {
    protected HashMap<String, NeuralNetworkNode> _nodeList = new HashMap<>();
    protected int _numberOfInputs = 0;
    protected int _numberOfOutputs = 0;

    /**
     * Makes a network off of the existing string to be loaded in.
     * @param existingNetwork the string of the existing network
     */
    public NeuralNetworkCore(String existingNetwork){
        String[] newNetwork = existingNetwork.split("\n");
        for (String node: newNetwork){
            if (node != null) {
                String[] nodeData = node.split("\\|");
                nodeData = nodeData[0].split(":");
                addNode(nodeData[0], Integer.parseInt(nodeData[1]));
            }
        }
        for(String node: newNetwork){
            if (node != null && node.split("\\|").length > 1) {
                String[] connections = node.split("\\|")[1].split(":");
                String currentNode = node.split("\\|")[0].split(":")[0];
                for(String connection: connections){
                    if(connection != "") {
                        String[] connectionData = connection.split(",");
                        addConnection(connectionData[0],
                                currentNode, Double.parseDouble(connectionData[1]));
                    }
                }
            }
        }
        _numberOfInputs = getNodesWithName(getNeuralNetworkCoin().get(0),
                getNodesOfType(NeuralNetworkNode.INPUT_NODE)).size();

        _numberOfOutputs = getNodesWithName(getNeuralNetworkCoin().get(0),
                getNodesOfType(NeuralNetworkNode.OUTPUT_NODE)).size();
    }

    /**
     * Makes a new neural network off of the input and output number of nodes
     * @param inputAmountPerCoin the number of inputs to the network
     * @param outputAmountPerCoin the number of outputs to the network
     */
    public NeuralNetworkCore(int inputAmountPerCoin, int outputAmountPerCoin, String[] coinNames){
        _numberOfInputs = inputAmountPerCoin;
        _numberOfOutputs = outputAmountPerCoin;
        for(String coin: coinNames) {
            addNewCoinInAndOut(coin);
        }
        addNode("WALLET-USD-0", NeuralNetworkNode.INPUT_NODE);
        for(int i= 0; i < outputAmountPerCoin; i++) {
            addNode("USD-O-" + i, NeuralNetworkNode.OUTPUT_NODE);
        }
    }

    /**
     * Gets the results of the neural network after given the number of inputs
     * Gets the results of the neural network after given the number of inputs
     * @param history the input values for the network
     * @param walletValues the values needed for the wallet.
     * @return an array of the list return values
     */
    public HashMap<String, double[]> getResults(HashMap<String, double[]> history, HashMap<String, Double> walletValues){
        HashMap<String, double[]> retVal = new HashMap<>();
        setWalletValues(walletValues);
        for(String crypto: history.keySet()) {
            for (int i = 0; i < _numberOfInputs; i++) {
                getNodeFromId(crypto + "-I-" + i).setValue(history.get(crypto)[i]);
            }
        }

        for(String crypto: history.keySet()) {
            double[] nodeResults = new double[_numberOfOutputs];
            for (int i = 0; i < _numberOfOutputs; i++) {
                nodeResults[i] = getNodeFromId(crypto + "-O-" + i).getCalculatedValue();
            }
            retVal.put(crypto, nodeResults);
        }

        double[] usdResults = new double[_numberOfOutputs];
        for(int i = 0; i < _numberOfOutputs; i++){
            usdResults[i] = getNodeFromId("USD-O-" + i).getCalculatedValue();
        }
        retVal.put("USD", usdResults);
        clearSystem();
        return retVal;
    }

    public void setWalletValues(HashMap<String, Double> walletValues){
        for(String coin: walletValues.keySet()){
            getNodeFromId("WALLET-" + coin + "-0").setValue(walletValues.get(coin));
        }
    }

    public ArrayList<NeuralNetworkNode> getNodesWithName(String name, ArrayList<NeuralNetworkNode> listOfNodes){
        for(int i = listOfNodes.size()-1; i >= 0; i--){
            if(!listOfNodes.get(i).getNodeId().split("-")[0].matches(name)){
                listOfNodes.remove(i);
            }
        }
        return listOfNodes;
    }

    public boolean isValidCryptoList(ArrayList<String> cryptoList){
        ArrayList<String> tempList = getNeuralNetworkCoin();
        for(int i = cryptoList.size() - 1; i >= 0; i--){
            if(tempList.contains(cryptoList.get(i))) {
                tempList.remove(cryptoList.get(i));
            }
            else{
                return false;
            }
        }
        return tempList.size() == 0;
    }

    public ArrayList<String> getNeuralNetworkCoin(){
        ArrayList<String> retValue = new ArrayList<>();
        for(NeuralNetworkNode node: _nodeList.values()){
            if(!node.getNodeId().split("-")[0].matches("HIDDEN") && !node.getNodeId().split("-")[0].matches("WALLET") &&
                    !retValue.contains(node.getNodeId().split("-")[0])){
                retValue.add(node.getNodeId().split("-")[0]);
            }
        }
        return retValue;
    }

    /**
     * Add a new node to the list of preexisting nodes
     * @param id the id of the new node that will be created
     * @param type the type of node this will be an int it is contained in NeuralNetwork.NeuralNetworkNode
     * @return the node that was created
     */
    public NeuralNetworkNode addNode(String id, int type){
        NeuralNetworkNode newNode = new NeuralNetworkNode(id, type);
        _nodeList.put(id, newNode);
        return newNode;
    }

    public void addNewCoinInAndOut(String name){
        for (int i = 0; i < _numberOfInputs; i++) {
            addNode(getNewId(name + "-I"), NeuralNetworkNode.INPUT_NODE);
        }
        for (int i = 0; i < _numberOfOutputs; i++) {
            addNode(getNewId(name + "-O"), NeuralNetworkNode.OUTPUT_NODE);
        }
        addNode("WALLET-" + name + "-0", NeuralNetworkNode.INPUT_NODE);
    }

    /**
     * add a new connection between two nodes
     * @param startingID the id that will be set as the target
     * @param endingID the one that will actually contain the system
     * @param weight the weight between the two nodes
     */
    public void addConnection(String startingID, String endingID, double weight){
        NeuralNetworkNode startingNode = getNodeFromId(startingID);
        NeuralNetworkNode endingNode = getNodeFromId(endingID);
        if(startingNode == null || endingNode == null)
            throw new IndexOutOfBoundsException();
        endingNode.addNodeConnection(startingNode, weight);
    }

    /**
     * Gets the string of the entire system
     * @return the string that can be used to recreate this network
     */
    @Override
    public String toString(){
        String retString = "";
        for(String node: _nodeList.keySet()){
            retString = retString + _nodeList.get(node).toString() + "\n";
        }
        return retString;
    }

    /**
     * this will get the node from a specified id
     * @param id the targeted node
     * @return the node data from the specified unique id
     */
    protected NeuralNetworkNode getNodeFromId(String id){
        return _nodeList.get(id);
    }

    /**
     * Gets all the nodes of a specified type
     * @param type the value to tag out
     * @return the array list of the values
     */
    public ArrayList<NeuralNetworkNode> getNodesOfType(int type){
        ArrayList<NeuralNetworkNode> retNodeList = new ArrayList<>();
        for(NeuralNetworkNode node: _nodeList.values()){
            if(type == node.getNodeType()) retNodeList.add(node);
        }
        return retNodeList;
    }

    public int getNumberOfInputs(){
        return _numberOfInputs;
    }

    public int getNumberOfOutputs(){
        return _numberOfOutputs;
    }

    /**
     * This makes a unique id for a new node. It can also handle a node being deleted
     * @param coin theName to be added at the start
     * @return an int of the new id
     */
    protected String getNewId(String coin){
        ArrayList<NeuralNetworkNode> nodesWithCoin = new ArrayList<>();
        for(String node: _nodeList.keySet()){
            if(_nodeList.get(node).getNodeId().contains(coin)){
                nodesWithCoin.add(_nodeList.get(node));
            }
        }
        boolean[] usedNumbers = new boolean[nodesWithCoin.size()];
        for(NeuralNetworkNode node: nodesWithCoin){
            String data = node.getNodeId().split("-")[node.getNodeId().split("-").length - 1];
            int nodeNumbers = Integer.parseInt(data);
            if(nodeNumbers < usedNumbers.length) usedNumbers[nodeNumbers] = true;
        }
        for(int i = 0; i < usedNumbers.length; i ++){
            if(!usedNumbers[i]) return coin + "-" + i;
        }
        return coin + "-" + nodesWithCoin.size();
    }

    /**
     * This is a system to clear the data system.
     */
    private void clearSystem(){
        for(NeuralNetworkNode node: _nodeList.values()){
            node.clearSetValue();
        }
    }
}
