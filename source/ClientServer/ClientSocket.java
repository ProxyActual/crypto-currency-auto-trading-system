package ClientServer;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class ClientSocket implements Runnable{
    private Socket _connectionSocket;
    private BufferedReader _reader;
    private PrintWriter _writer;
    private ArrayList<String> _newData;
    private Thread _thisThread;
    private boolean _running = true;

    /**
     * The creation of a socket it does need a socket for the creation
     * @param socket the socket for the creation
     */
    public ClientSocket(Socket socket){
        _connectionSocket = socket;
        initiateConnections();
    }

    /**
     * This makes a socket that has the ability to connect to the destination location
     * @param destination the location of the server
     * @param port the port open on the server
     */
    public ClientSocket(String destination, int port){
        try {
            _connectionSocket = new Socket(destination, port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        initiateConnections();
    }

    /**
     * The core loops of the system
     */
    @Override
    public void run(){
        while(_running){
            try {
                int newIn = _reader.read();
                if(newIn == -1)
                    killSocket();
                String newData = (char)newIn + _reader.readLine();
                _newData.add(newData);
                Thread.sleep(250);
            } catch (IOException | InterruptedException e) {
                killSocket();
            }
        }
        killSocket();
    }

    /**
     * This will kill the socket. ending the connection on the other system as well
     */
    public void killSocket(){
        _running = false;
        _thisThread.interrupt();
        try {
            _connectionSocket.close();
            _reader.close();
            _writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * get the new inputs from the buffer of the system.
     * @return a list of new values. It should be known that after this the buffer is empty
     */
    public String[] getNewInputs(){
        String[] retVal = _newData.toArray(new String[_newData.size()]);
        _newData = new ArrayList<>();
        return retVal;
    }

    /**
     * sends the data across the socket
     * @param newData the new data to send across the socket
     */
    public void sendData(String newData){
        _writer.println(newData);
        _writer.flush();
    }

    /**
     * Tests to see if the socket is still connected across the system
     * @return ture if there is still an active connection
     */
    public boolean isConnected(){
        return _running;
    }

    /**
     * This will start the new readers and writers and the thread to read and write the system
     */
    private void initiateConnections(){
        try {
             _writer = new PrintWriter(new OutputStreamWriter(_connectionSocket.getOutputStream()));
             _reader = new BufferedReader(new InputStreamReader(_connectionSocket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        _newData = new ArrayList<>();
        _thisThread = new Thread(this);
        _thisThread.start();
    }
}
